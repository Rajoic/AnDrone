package ce301.essex.androne.drone;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM;
import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DICTIONARY_KEY_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_ERROR_ENUM;
import com.parrot.arsdk.arcontroller.ARControllerArgumentDictionary;
import com.parrot.arsdk.arcontroller.ARControllerCodec;
import com.parrot.arsdk.arcontroller.ARControllerDictionary;
import com.parrot.arsdk.arcontroller.ARControllerException;
import com.parrot.arsdk.arcontroller.ARDeviceController;
import com.parrot.arsdk.arcontroller.ARDeviceControllerListener;
import com.parrot.arsdk.arcontroller.ARDeviceControllerStreamListener;
import com.parrot.arsdk.arcontroller.ARFeatureARDrone3;
import com.parrot.arsdk.arcontroller.ARFeatureCommon;
import com.parrot.arsdk.arcontroller.ARFrame;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_ENUM;
import com.parrot.arsdk.ardiscovery.ARDISCOVERY_PRODUCT_FAMILY_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDevice;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryException;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;
import com.parrot.arsdk.arutils.ARUTILS_DESTINATION_ENUM;
import com.parrot.arsdk.arutils.ARUTILS_FTP_TYPE_ENUM;
import com.parrot.arsdk.arutils.ARUtilsException;
import com.parrot.arsdk.arutils.ARUtilsManager;

import java.util.ArrayList;
import java.util.List;

public class BebopDrone {

    private static final String TAG = BebopDrone.class.getSimpleName();

    public interface Listener {
        /**
         * Called when the connection to the drone changes
         * Called in the main thread
         * @param state the state of the drone
         */
        void onDroneConnectionChanged(ARCONTROLLER_DEVICE_STATE_ENUM state);

        /**
         * Called when the battery charge changes
         * Called in the main thread
         * @param batteryPercentage the battery remaining (in percent)
         */
        void onBatteryChargeChanged(int batteryPercentage);

        /**
         * Called when the piloting state changes
         * Called in the main thread
         * @param state the piloting state of the drone
         */
        void onPilotingStateChanged(ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state);

        /**
         * Called when a picture is taken
         * Called on a separate thread
         * @param error ERROR_OK if picture has been taken, otherwise describe the error
         */
        void onPictureTaken(ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error);

        /**
         * Called when the video decoder should be configured
         * Called on a separate thread
         * @param codec the codec to configure the decoder with
         */
        void configureDecoder(ARControllerCodec codec);

        /**
         * Called when a video frame has been received
         * Called on a separate thread
         * @param frame the video frame
         */
        void onFrameReceived(ARFrame frame);

        /**
         * Called before medias will be downloaded
         * Called in the main thread
         * @param nbMedias the number of medias that will be downloaded
         */
        void onMatchingMediasFound(int nbMedias);

        /**
         * Called each time the progress of a download changes
         * Called in the main thread
         * @param mediaName the name of the media
         * @param progress the progress of its download (from 0 to 100)
         */
        void onDownloadProgressed(String mediaName, int progress);

        /**
         * Called when a media download has ended
         * Called in the main thread
         * @param mediaName the name of the media
         */
        void onDownloadComplete(String mediaName);
    }

    private final List<Listener> LISTENERS;

    private final Handler HANDLER;
    private final Context CONTEXT;

    private ARDeviceController deviceController;
    private SDCardModule module;
    private ARCONTROLLER_DEVICE_STATE_ENUM state;
    private ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM flyingState;
    private String currentRunId;
    private ARDiscoveryDeviceService deviceService;
    private ARUtilsManager ftpListManager;
    private ARUtilsManager ftpQueueManager;

    public BebopDrone(Context context, @NonNull ARDiscoveryDeviceService deviceService) {

        CONTEXT = context;
        LISTENERS = new ArrayList<>();
        this.deviceService = deviceService;

        // needed because some callbacks will be called on the main thread
        HANDLER = new Handler(context.getMainLooper());

        state = ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_STOPPED;

        // if the product type of the deviceService match with the types supported
        ARDISCOVERY_PRODUCT_ENUM productType = ARDiscoveryService.getProductFromProductID( this.deviceService.getProductID() );
        ARDISCOVERY_PRODUCT_FAMILY_ENUM family = ARDiscoveryService.getProductFamily( productType );
        if( ARDISCOVERY_PRODUCT_FAMILY_ENUM.ARDISCOVERY_PRODUCT_FAMILY_ARDRONE.equals( family ) ) {

            ARDiscoveryDevice discoveryDevice = createDiscoveryDevice( this.deviceService );
            if( discoveryDevice != null ) {
                deviceController = createDeviceController(discoveryDevice);
                discoveryDevice.dispose();
            }

            try {
                ftpListManager = new ARUtilsManager();
                ftpQueueManager = new ARUtilsManager();

                ftpListManager.initFtp(  CONTEXT, deviceService, ARUTILS_DESTINATION_ENUM.ARUTILS_DESTINATION_DRONE, ARUTILS_FTP_TYPE_ENUM.ARUTILS_FTP_TYPE_GENERIC );
                ftpQueueManager.initFtp( CONTEXT, deviceService, ARUTILS_DESTINATION_ENUM.ARUTILS_DESTINATION_DRONE, ARUTILS_FTP_TYPE_ENUM.ARUTILS_FTP_TYPE_GENERIC );

                module = new SDCardModule( ftpListManager, ftpQueueManager );
                module.addListener( mSDCardModuleListener );
            }
            catch( ARUtilsException e ) {
                Log.e( TAG, "Exception", e );
            }

        } else {
            Log.e( TAG, "DeviceService type is not supported by BebopDrone" );
        }
    }

    // Dispose of instance variables
    public void dispose() {
        if( deviceController != null ) deviceController.dispose();
        if( ftpListManager   != null ) ftpListManager.closeFtp(  CONTEXT, deviceService );
        if( ftpQueueManager  != null ) ftpQueueManager.closeFtp( CONTEXT, deviceService );
    }

    //region Listener functions
    // Add a listener
    public void addListener( Listener listener ) {
        LISTENERS.add( listener );
    }

    // Remove a listener
    public void removeListener( Listener listener ) {
        LISTENERS.remove( listener );
    }
    //endregion Listener

    /**
     * Connect to the drone
     * @return true if operation was successful.
     *              Returning true doesn't mean that device is connected.
     *              You can be informed of the actual connection through {@link Listener#onDroneConnectionChanged}
     */
    public boolean connect() {
        boolean success = false;
        // If the drone isn't running and a controller has been initialised
        if( ( deviceController != null ) && ( ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_STOPPED.equals( state ) ) ) {
            // Try to start the drone
            ARCONTROLLER_ERROR_ENUM error = deviceController.start();
            if( error == ARCONTROLLER_ERROR_ENUM.ARCONTROLLER_OK ) {
                success = true;
            }
        }
        // Return true if the drone was started successfully. Otherwise return false
        return success;
    }

    /**
     * Disconnect from the drone
     * @return true if operation was successful.
     *              Returning true doesn't mean that device is disconnected.
     *              You can be informed of the actual disconnection through {@link Listener#onDroneConnectionChanged}
     */
    public boolean disconnect() {
        boolean success = false;
        // if( deviceController != null ) && ( ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING.equals( state ) ) {
        if( isStateAcceptable() ) {
            // Try and stop the drone
            ARCONTROLLER_ERROR_ENUM error = deviceController.stop();
            // Check if the error code returned after stopping the controller confirms the controller has been successfully stopped
            if( error == ARCONTROLLER_ERROR_ENUM.ARCONTROLLER_OK ) {
                success = true;
            }
        }
        // return the result
        return success;
    }

    /**
     * Get the current connection state
     * @return the connection state of the drone
     */
    public ARCONTROLLER_DEVICE_STATE_ENUM getConnectionState() {
        return state;
    }

    /**
     * Get the current flying state
     * @return the flying state
     */
    public ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM getFlyingState() {
        return flyingState;
    }

    // Checking if the drone is flying and that the controller has been initialised
    private boolean isStateAcceptable() {
        if( ( deviceController != null ) && ( state.equals( ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING ) ) )
            return true;
        else return false;
    }

    // Make the drone take off
    public void takeOff() {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().sendPilotingTakeOff();
        }
    }

    // Make the drone land
    public void land() {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().sendPilotingLanding();
        }
    }

    // Making the drone perform an emergency landing
    public void emergency() {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().sendPilotingEmergency();
        }
    }

    // Taking a picture of the drones camera feed and saving it to the drones on-board storage
    public void takePicture() {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().sendMediaRecordPictureV2();
        }
    }

    /**
     * Set the forward/backward angle of the drone
     * Note that {@link BebopDrone#setFlag(byte)} should be set to 1 in order to take in account the pitch value
     * @param pitch value in percentage from -100 to 100
     */
    // Setting the pitch velocity of the drone
    public void setPitch( byte pitch ) {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().setPilotingPCMDPitch( pitch );
        }
    }

    /**
     * Set the side angle of the drone
     * Note that {@link BebopDrone#setFlag(byte)} should be set to 1 in order to take in account the roll value
     * @param roll value in percentage from -100 to 100
     */
    // Setting the roll velocity of the drone
    public void setRoll( byte roll ) {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().setPilotingPCMDRoll( roll );
        }
    }

    // Setting the yaw velocity of the drone
    public void setYaw( byte yaw ) {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().setPilotingPCMDYaw( yaw );
        }
    }

    // Setting the altitude speed of the drone
    public void setGaz(byte gaz) {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().setPilotingPCMDGaz( gaz );
        }
    }

    /**
     * Take in account or not the pitch and roll values
     * @param flag 1 if the pitch and roll values should be used, 0 otherwise
     */
    public void setFlag( byte flag ) {
        if( isStateAcceptable() ) {
            deviceController.getFeatureARDrone3().setPilotingPCMDFlag( flag) ;
        }
    }

    /**
     * Download the last flight medias
     * Uses the run id to download all medias related to the last flight
     * If no run id is available, download all medias of the day
     */
    public void getLastFlightMedias() {
        String runId = currentRunId;
        if( ( runId != null ) && !runId.isEmpty() ) {
            module.getFlightMedias( runId );
        } else {
            Log.e( TAG, "RunID not available, fallback to the day's medias" );
            module.getTodaysFlightMedias();
        }
    }

    public void cancelGetLastFlightMedias() {
        module.cancelGetFlightMedias();
    }

    // Create an ARDiscoveryDevice object from the service object
    private ARDiscoveryDevice createDiscoveryDevice( @NonNull ARDiscoveryDeviceService service ) {
        ARDiscoveryDevice device = null;
        try {
            device = new ARDiscoveryDevice(CONTEXT, service);
        } catch( ARDiscoveryException e ) {
            Log.e( TAG, "Exception", e );
            Log.e( TAG, "Error: " + e.getError() );
        }

        return device;
    }

    private ARDeviceController createDeviceController( @NonNull ARDiscoveryDevice discoveryDevice ) {
        ARDeviceController deviceController = null;
        try {
            deviceController = new ARDeviceController( discoveryDevice );

            deviceController.addListener( mDeviceControllerListener );
            deviceController.addStreamListener( mStreamListener );
        } catch( ARControllerException e ) {
            Log.e( TAG, "Exception", e );
        }

        return deviceController;
    }

    //region notify listener block
    private void notifyConnectionChanged( ARCONTROLLER_DEVICE_STATE_ENUM state ) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.onDroneConnectionChanged( state );
        }
    }

    private void notifyBatteryChanged( int battery ) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.onBatteryChargeChanged( battery );
        }
    }

    private void notifyPilotingStateChanged( ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state ) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.onPilotingStateChanged( state );
        }
    }

    private void notifyPictureTaken( ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error ) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.onPictureTaken( error );
        }
    }

    private void notifyConfigureDecoder( ARControllerCodec codec ) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.configureDecoder( codec );
        }
    }

    private void notifyFrameReceived( ARFrame frame ) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.onFrameReceived( frame );
        }
    }

    private void notifyMatchingMediasFound(int nbMedias) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for (Listener listener : listenersCpy) {
            listener.onMatchingMediasFound(nbMedias);
        }
    }

    private void notifyDownloadProgressed(String mediaName, int progress) {
        List<Listener> listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.onDownloadProgressed( mediaName, progress );
        }
    }

    private void notifyDownloadComplete( String mediaName ) {
        List< Listener > listenersCpy = new ArrayList<>( LISTENERS );
        for( Listener listener : listenersCpy ) {
            listener.onDownloadComplete( mediaName );
        }
    }
    //endregion notify listener block

    private final SDCardModule.Listener mSDCardModuleListener = new SDCardModule.Listener() {
        @Override
        public void onMatchingMediasFound( final int nbMedias ) {
            HANDLER.post( new Runnable() {
                @Override
                public void run() {
                    notifyMatchingMediasFound( nbMedias );
                }
            } );
        }

        @Override
        public void onDownloadProgressed( final String mediaName, final int progress ) {
            HANDLER.post( new Runnable() {
                @Override
                public void run() {
                    notifyDownloadProgressed( mediaName, progress );
                }
            } );
        }

        @Override
        public void onDownloadComplete( final String mediaName ) {
            HANDLER.post( new Runnable() {
                @Override
                public void run() {
                    notifyDownloadComplete( mediaName );
                }
            } );
        }
    };

    private final ARDeviceControllerListener mDeviceControllerListener = new ARDeviceControllerListener() {
        @Override
        public void onStateChanged( ARDeviceController deviceController, ARCONTROLLER_DEVICE_STATE_ENUM newState, ARCONTROLLER_ERROR_ENUM error ) {
            state = newState;
            if( ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING.equals( state ) ) {
                BebopDrone.this.deviceController.startVideoStream();
            } else if( ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_STOPPED.equals( state ) ) {
                module.cancelGetFlightMedias();
            }
            HANDLER.post( new Runnable() {
                @Override
                public void run() {
                    notifyConnectionChanged( state );
                }
            } );
        }

        @Override
        public void onExtensionStateChanged( ARDeviceController deviceController, ARCONTROLLER_DEVICE_STATE_ENUM newState, ARDISCOVERY_PRODUCT_ENUM product, String name, ARCONTROLLER_ERROR_ENUM error ) {
        }

        @Override
        public void onCommandReceived(ARDeviceController deviceController, ARCONTROLLER_DICTIONARY_KEY_ENUM commandKey, ARControllerDictionary elementDictionary ) {
            // if event received is the battery update
            if( ( commandKey == ARCONTROLLER_DICTIONARY_KEY_ENUM.ARCONTROLLER_DICTIONARY_KEY_COMMON_COMMONSTATE_BATTERYSTATECHANGED ) && ( elementDictionary != null ) ) {
                ARControllerArgumentDictionary< Object > args = elementDictionary.get( ARControllerDictionary.ARCONTROLLER_DICTIONARY_SINGLE_KEY );
                if( args != null ) {
                    final int battery = ( Integer ) args.get( ARFeatureCommon.ARCONTROLLER_DICTIONARY_KEY_COMMON_COMMONSTATE_BATTERYSTATECHANGED_PERCENT );
                    HANDLER.post( new Runnable() {
                        @Override
                        public void run() {
                            notifyBatteryChanged( battery );
                        }
                    } );
                }
            }
            // if event received is the flying state update
            else if( ( commandKey == ARCONTROLLER_DICTIONARY_KEY_ENUM.ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED ) && ( elementDictionary != null ) ) {
                ARControllerArgumentDictionary< Object > args = elementDictionary.get( ARControllerDictionary.ARCONTROLLER_DICTIONARY_SINGLE_KEY );
                if( args != null ) {
                    final ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state = ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM.getFromValue( ( Integer ) args.get( ARFeatureARDrone3.ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE ) );

                    HANDLER.post( new Runnable() {
                        @Override
                        public void run() {
                            flyingState = state;
                            notifyPilotingStateChanged( state );
                        }
                    } );
                }
            }
            // if event received is the picture notification
            else if( ( commandKey == ARCONTROLLER_DICTIONARY_KEY_ENUM.ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED ) && ( elementDictionary != null ) ){
                ARControllerArgumentDictionary< Object > args = elementDictionary.get( ARControllerDictionary.ARCONTROLLER_DICTIONARY_SINGLE_KEY );
                if( args != null ) {
                    final ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error = ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM.getFromValue( ( Integer )args.get( ARFeatureARDrone3.ARCONTROLLER_DICTIONARY_KEY_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR ) );
                    HANDLER.post( new Runnable() {
                        @Override
                        public void run() {
                            notifyPictureTaken( error );
                        }
                    } );
                }
            }
            // if event received is the run id
            else if( ( commandKey == ARCONTROLLER_DICTIONARY_KEY_ENUM.ARCONTROLLER_DICTIONARY_KEY_COMMON_RUNSTATE_RUNIDCHANGED) && (elementDictionary != null)){
                ARControllerArgumentDictionary< Object > args = elementDictionary.get( ARControllerDictionary.ARCONTROLLER_DICTIONARY_SINGLE_KEY );
                if (args != null) {
                    final String runID = ( String ) args.get( ARFeatureCommon.ARCONTROLLER_DICTIONARY_KEY_COMMON_RUNSTATE_RUNIDCHANGED_RUNID );
                    HANDLER.post( new Runnable() {
                        @Override
                        public void run() {
                            currentRunId = runID;
                        }
                    } );
                }
            }
        }
    };

    private final ARDeviceControllerStreamListener mStreamListener = new ARDeviceControllerStreamListener() {
        @Override
        public ARCONTROLLER_ERROR_ENUM configureDecoder( ARDeviceController deviceController, final ARControllerCodec codec ) {
            notifyConfigureDecoder( codec );
            return ARCONTROLLER_ERROR_ENUM.ARCONTROLLER_OK;
        }

        @Override
        public ARCONTROLLER_ERROR_ENUM onFrameReceived( ARDeviceController deviceController, final ARFrame frame ) {
            notifyFrameReceived( frame );
            return ARCONTROLLER_ERROR_ENUM.ARCONTROLLER_OK;
        }

        @Override
        public void onFrameTimeout( ARDeviceController deviceController ) {}
    };
}
