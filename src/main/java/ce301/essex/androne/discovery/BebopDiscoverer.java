package ce301.essex.androne.discovery;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiver;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiverDelegate;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import ce301.essex.androne.controller.*;
import ce301.essex.androne.menu.*;

public class BebopDiscoverer {

    private static final String TAG = BebopDiscoverer.class.getSimpleName();

    // Listener for detecting when a new drone is found and responding to it appropriately
    public interface Listener {

        // Method called when the list of drones detected and available to control is updated.
        // It should be called in the menu thread takes a list of drone service objects representing
        // all available drones
        void onDronesListUpdated(List<ARDiscoveryDeviceService> dronesList);
    }

    private final List< Listener > LISTENERS;
    private final Context CONTEXT;
    private MainMenuActivity menu;
    private Timer timer;

    private final ARDiscoveryServicesDevicesListUpdatedReceiver RECEIVER;
    private final List<ARDiscoveryDeviceService> MATCHING_DRONES;
    private final ARDiscoveryServicesDevicesListUpdatedReceiverDelegate discoveryListener;

    private ARDiscoveryService discoveryDevice;
    private ServiceConnection serviceConnection;

    private boolean startDiscovering;

    // Constructor for the BebopDiscoverer class which initialises some final class variables.
    // Takes a Context object as an argument containing the context of the MainMenuActivity
    // instance.
    public BebopDiscoverer( MainMenuActivity menu, Context context, Timer timer ) {

        this.timer = timer; // Timer object from the main menu activity
        this.menu = menu; // Main menu activity instance
        CONTEXT = context; // Storing the MainMenuActivity context
        LISTENERS = new ArrayList<>(); // creating an ArrayList of Listener objects
        MATCHING_DRONES = new ArrayList<>(); // Creating an ArrayList of discovered drones

        // Creating discovery listener for finding drones to fly and starting the next activity
        discoveryListener = new ARDiscoveryServicesDevicesListUpdatedReceiverDelegate() {
            @Override
            public void onServicesDevicesListUpdated() {
                if( discoveryDevice != null ) {
                    // Clear current list
                    MATCHING_DRONES.clear();
                    List< ARDiscoveryDeviceService > deviceList = discoveryDevice.getDeviceServicesArray();

                    if( deviceList != null ) {
                        for (ARDiscoveryDeviceService service : deviceList) {
                            if( service != null ) {
                                // Create a new activity for flying the newly found bebop drone
                                Intent intent = new Intent( CONTEXT, DroneControllerActivity.class );
                                intent.putExtra( MainMenuActivity.EXTRA_BEBOP_SERVICE, service );
                                // Close dialog box
                                BebopDiscoverer.this.menu.connectDialog.setOnDismissListener( null );
                                BebopDiscoverer.this.menu.connectDialog.dismiss();
                                BebopDiscoverer.this.timer.cancel();
                                BebopDiscoverer.this.timer.purge();
                                CONTEXT.startActivity( intent );
                            }
                            MATCHING_DRONES.add( service );
                        }
                    }
                    notifyServiceDiscovered( MATCHING_DRONES );
                }
            }
        };

        // Creating a receiver object which listens for new drones added to the list of
        // discovered drones
        RECEIVER = new ARDiscoveryServicesDevicesListUpdatedReceiver( discoveryListener );


    }

    // Adds a listener to the list of listeners and notifies the MainMenuActivity that a new
    // drone has been found
    public void addListener( Listener listener ) {
        Log.i( TAG, "adding listener" );
        LISTENERS.add( listener );
        notifyServiceDiscovered( MATCHING_DRONES );
    }

    // Removes a listener from the list of listeners
    public void removeListener( Listener listener ) { LISTENERS.remove(listener); }

    // Setup method that sets up this instance of the BebopDiscoverer class.
    public void setup() {
        Log.i( TAG, "setup" );

        // Register receivers
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance( CONTEXT );
        manager.registerReceiver( RECEIVER,
                new IntentFilter( ARDiscoveryService.kARDiscoveryServiceNotificationServicesDevicesListUpdated ) );

        // Create the service connection if it doesn't exist
        if( serviceConnection == null ) {
            Log.i( TAG, "setup new serviceConnection" );
            serviceConnection = new ServiceConnection() {

                // On service connected, create the discovery device and start looking for drones
                // to connect to
                @Override
                public void onServiceConnected(ComponentName name, IBinder service ) {
                    discoveryDevice = ( ( ARDiscoveryService.LocalBinder ) service ).getService();
                    if( startDiscovering ) {
                        startDiscovering();
                        startDiscovering = false;
                    }
                }

                // If the service is disconnected, remove the instance of the discoveryDevice object
                @Override
                public void onServiceDisconnected( ComponentName name ) {
                    discoveryDevice = null;
                }
            };
        }

        // If the discoveryDevice doesn't exist, bind the service connection to the context of the
        // MainMenuActivity
        if( discoveryDevice == null ) {
            Log.i( TAG, "setup bind ARDiscoveryService" );
            Intent i = new Intent( CONTEXT, ARDiscoveryService.class );
            CONTEXT.bindService( i, serviceConnection, Context.BIND_AUTO_CREATE );
        }
    }

    // Cleanup method to clean this instance of the BebopDiscoverer when it isn't being used anymore
    public void cleanup() {
        Log.d( TAG, "Cleanup..." );
        stopDiscovering();

        // If the discoveryDevice exists, create a new thread ot stop it, unbind it and remove it
        if( discoveryDevice != null ) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    discoveryDevice.stop();
                    CONTEXT.unbindService( serviceConnection );
                    discoveryDevice = null;
                }
            } ).start();
        }

        // Unregister the receivers
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance( CONTEXT );
        manager.unregisterReceiver( RECEIVER );
    }

    // Starts discovering drones that the Android device is connected to. When a drone is found, the
    // android device will be notified throught the onDronesListUpdated method
    public void startDiscovering() {
        // If the discoveryDevice object exists, update the drones list and start discovering.
        // Otherwise, set the startDiscovering flag to true so it creates a discoveryDevice when able
        if( discoveryDevice != null ) {
            Log.i( TAG, "startdiscovering: discoveryService != null" );
            discoveryListener.onServicesDevicesListUpdated();
            discoveryDevice.start();
            startDiscovering = false;
        } else {
            Log.i( TAG,"startDiscovering: discoveryService == null" );
            startDiscovering = true;
        }
    }

    // Stops the drone discovery process if the discoveryDevice exists and sets the startDiscovery
    // flag to false
    public void stopDiscovering() {
        if( discoveryDevice != null ) {
            Log.i( TAG, "Stop discovering" );
            System.out.println( "Stop discovering" );
            discoveryDevice.stop();
        }
        startDiscovering = false;
    }

    // Notifies the android device that a new drone has been found or lost by updating the list of
    // drones available
    private void notifyServiceDiscovered( List< ARDiscoveryDeviceService > dronesList ) {
        Log.i(TAG, "notifyServiceDiscovered");
        System.out.println("notifyServiceDiscovered");
        List<Listener> listenersCopy = new ArrayList<>(LISTENERS);
        for (Listener listener : listenersCopy) {
            Log.i(TAG, "adding listener to drones list");
            System.out.println("adding listener to drones list");
            listener.onDronesListUpdated(dronesList);
        }
    }
}
