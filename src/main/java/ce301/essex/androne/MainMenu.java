package ce301.essex.androne;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.parrot.arsdk.ARSDK;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;

public class MainMenu extends AppCompatActivity {

    MainButtonListener buttonListener;
    Button connect;
    Button exit;
    RadioGroup group;

    private ARDiscoveryService discoveryService;
    private ServiceConnection serviceConnection;

    private void initDiscoveryService() {
        if( this.serviceConnection == null ) {
            this.serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected( ComponentName name, IBinder service ) {
                    discoveryService = ( ( ARDiscoveryService.LocalBinder ) service).getService();
                    startDiscovery();
                }

                @Override
                public void onServiceDisconnected( ComponentName name ) {
                    discoveryService = null;
                }
            };
        }

        if( discoveryService == null ) {
            Intent discoveryIntent = new Intent( getApplicationContext(),
                    ARDiscoveryService.class );
            getApplicationContext().bindService( discoveryIntent,
                    serviceConnection, Context.BIND_AUTO_CREATE );
        }
    }

    private void startDiscovery() {
        if( this.discoveryService != null )
            this.discoveryService.start();
    }

    static {
        ARSDK.loadSDKLibs();
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main_landscape );

        buttonListener = new MainButtonListener( this );

        connect = ( Button ) findViewById( R.id.buttonConnect );
        exit = ( Button ) findViewById( R.id.buttonExit );
        group = ( RadioGroup ) findViewById( R.id.radioGroup );

        connect.setOnClickListener( buttonListener );
        exit.setOnClickListener( buttonListener );
        group.setOnCheckedChangeListener( new OrientationListener( this ) );
    }

    public boolean connect() {
        return false;
    }
}

class MainButtonListener implements View.OnClickListener {

    private MainMenu menu;

    public MainButtonListener( MainMenu menu ) {
        this.menu = menu;
    }
    @Override
    public void onClick( View view ) {
        switch( view.getId() ) {
            case R.id.buttonConnect:
                boolean connect = menu.connect();

                if( connect ) {
                    // controller activity
                }
                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder( menu );
                    builder.setMessage( "The drone was not found.\nPlease check the drone and try again." )
                           .setTitle( "Error" )
                           .setPositiveButton( "Continue", new DialogInterface.OnClickListener() {
                        public void onClick( DialogInterface dialog, int id ) {
                            dialog.dismiss();
                        }
                    } );
                    builder.create().show();
                }
                break;
            case R.id.buttonExit:
                System.exit( 0 );
                break;
        }
    }
}

class OrientationListener implements RadioGroup.OnCheckedChangeListener {

    MainMenu menu;

    public OrientationListener( MainMenu menu ) {
        this.menu = menu;
    }

    @Override
    public void onCheckedChanged( RadioGroup group, int checkedId ) {
        boolean checked = ((RadioButton) group.findViewById( checkedId )).isChecked();

        switch( checkedId ) {
            case R.id.radioVertical:
                if( checked ) {
                    menu.setContentView( R.layout.activity_main_portrait );
                } else {
                    menu.setContentView( R.layout.activity_main_landscape );
                }
                break;
            case R.id.radioHorizontal:
                if( checked ) {
                    menu.setContentView( R.layout.activity_main_portrait );
                } else {
                    menu.setContentView( R.layout.activity_main_landscape);
                }
                break;
        }
     }
}
