package ce301.essex.androne.menu;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.parrot.arsdk.ARSDK;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;

import org.opencv.core.Core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import ce301.essex.androne.R;
import ce301.essex.androne.discovery.BebopDiscoverer;

public class MainMenuActivity extends AppCompatActivity {

    // Tags for this this class and fragment classes
    private static final String TAG = MainMenuActivity.class.getSimpleName();
    private static final String TAG_FRAGMENT = MainMenuActivity.class.getSimpleName();

    // Extra service string
    public static final String EXTRA_BEBOP_SERVICE = "EXTRA_BEBOP_SERVICE";

    // Permission request code
    private static final int REQUEST_CODE_PERMISSIONS_REQUEST = 1;

    // Array of permissions needed
    private static final String[] PERMISSIONS_NEEDED = new String[] {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    // Orientation boolean. true == portrait, false == landscape
    public static Boolean orientationPortrait = null;

    // Fragment class for holding listeners and other variables
    private MainMenuFragment fragment;

    // Button variables
    private Button connect;
    private Button exit;
    private RadioGroup group;

    // Listeners for buttons
    private MainButtonListener buttonListener;
    private RadioListener radioListener;

    // Boolean for flagging activity. True == main menu is active. False == main menu is finished.
    private boolean active;

    //
    private final List< ARDiscoveryDeviceService > DRONES_LIST = new ArrayList<>();
    private ARDiscoveryService discoveryService;
    private ServiceConnection serviceConnection;

    // BebopDiscoverer instance for finding and connecting to the bebop drone.
    // You must be connected to the drone via Wi-Fi and have your mobile data
    // turned off for the connection to succeed.
    private BebopDiscoverer bebopDiscoverer;

    // public ProgressDialog connecting;
    public AlertDialog.Builder connectBuilder;
    public AlertDialog connectDialog;

    private final BebopDiscoverer.Listener DISCOVERER_LISTENER = new BebopDiscoverer.Listener() {

        // Method to update the list of drones to connect to
        // it's called when a new drone is found or one that was available is now lost
        @Override
        public void onDronesListUpdated(List<ARDiscoveryDeviceService> dronesList) {
            // Clearing list of old data and repopulating it with the new data.
            DRONES_LIST.clear();
            DRONES_LIST.addAll( dronesList );
        }
    };

    static {
        ARSDK.loadSDKLibs();
        try {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        } catch( Exception e ) {
            Log.e( TAG, "opencv", e );
        }
    }

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        // Requesting application permissions from the android system and the user
        requestPermissions();

        // If the portrait flag is null
        if( orientationPortrait == null ) {
            // Set the contentView to the portrait menu layout
            setContentView( R.layout.activity_main_menu_portrait );
            // Set the orientation to portrait
            this.setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT );
            // Set the portrait flag to true and check the portrait radio button
            orientationPortrait = true;
            ( ( RadioGroup ) findViewById( R.id.radio_group_orientation) ).check( R.id.radio_portrait );
        } // Otherwise, if the portrait flag is set to true
        else if( orientationPortrait ) {
            // Set the content view  to the portrait menu layout
            setContentView( R.layout.activity_main_menu_portrait);
            // Set the orientation to portrait
            this.setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT );
            // Set the portrait radio button to checked
            ( ( RadioGroup ) findViewById( R.id.radio_group_orientation) ).check( R.id.radio_portrait );
        } // Otherwise, if the portrait flag is set to false
        else if( !orientationPortrait ) {
            // Set the content view to the landscape menu layout
            setContentView( R.layout.activity_main_menu_landscape);
            // Set the orientation to landscape
            this.setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE );
            // Set the landscape radio button to checked
            ( ( RadioGroup ) findViewById( R.id.radio_group_orientation) ).check( R.id.radio_landscape );
        }

        // Set the active boolean flag to true
        active = true;

        // Retrieve the activity fragment from the FragmentManager
        fragment = ( MainMenuFragment ) getFragmentManager().findFragmentByTag( TAG_FRAGMENT );
        // If the fragment doesn't exist yet
        if( fragment == null ) {
            // Create a new fragment, set the Bundle object as an argument
            fragment = new MainMenuFragment();
            fragment.setArguments( savedInstanceState );
            // Add the fragment to the fragment manager
            getFragmentManager().beginTransaction().add( fragment, TAG_FRAGMENT ).commit();
        }

        // Get the connect button, exit button and orientation radio group
        connect = ( Button ) findViewById( R.id.connect_btn );
        exit = ( Button ) findViewById( R.id.exit_btn );
        group = ( RadioGroup ) findViewById( R.id.radio_group_orientation );
    }

    // On resume, set up the fragment and reapply the listeners for the buttons
    @Override
    protected void onResume() {
        super.onResume();

        // Using the fragment to set up this object and the button listeners
        fragment.setup( this );
        radioListener = fragment.getRadioListener();
        buttonListener = fragment.getButtonListener();

        // Setting the button listeners to the menu buttons and radio group
        connect.setOnClickListener( buttonListener );
        exit.setOnClickListener( buttonListener );
        group.setOnCheckedChangeListener( radioListener );
    }

    // On pause, clean the bebopDiscoverer for it to be reinitialised when it is resumed and remove
    // the fragment from the fragment manager.
    @Override
    protected void onPause() {
        super.onPause();

        if( bebopDiscoverer != null ) {
            bebopDiscoverer.stopDiscovering();
            bebopDiscoverer.cleanup();
            bebopDiscoverer.removeListener( DISCOVERER_LISTENER );
        }

        if( !active ) getFragmentManager().beginTransaction().remove( fragment ).commit();
    }

    // A method which checks which permissions have been granted and responds accordingly.
    // If all requested permissions have been granted, the application will continue as normal.
    // If at least 1 request has been denied, the application should close without an error, stating
    // that at least 1 request wasn't granted.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );

        boolean denied = false;
        // if no permissions have been granted, set denied to true
        if( permissions.length == 0 )
            denied = true;
        // Otherwise, check each permission to see if it has been denied. If one has been denied,
        // set denied to true
        else {
            for( int i = 0; i < permissions.length; i++ ) {
                if( grantResults[ i ] == PackageManager.PERMISSION_DENIED ) {
                    denied = true;
                    break;
                }
            }
        }

        // If a permission has been denied, exit the application stating that at least 1 permission is missing
        if( denied ) {
            Toast.makeText( this, "At least 1 of the requested permissions wasn't granted.", Toast.LENGTH_LONG ).show();
            finish();
        }

    }

    @Override
    protected void onSaveInstanceState( Bundle state ) {
        super.onSaveInstanceState( state );

        // Saving the portrait and active boolean flags in the Bundle object
        state.putBoolean( "orientationPortrait", orientationPortrait );
        state.putBoolean( "active", active );
    }

    @Override
    protected void onRestoreInstanceState( Bundle state ) {
        super.onRestoreInstanceState( state );

        // Getting the portrait and active boolean flags from the Bundle object
        orientationPortrait = state.getBoolean( "orientationPortrait" );
        active = state.getBoolean( "active" );
    }

    // A method which goes through the required permissions and requests them. If the permissions
    // have already been denied by the user once, the app will close and tell them to allow the
    // denied permission in order to continue using AnDrone.
    public void requestPermissions() {

        // Iterate through permissions and check if the needed permissions have already been granted
        Set< String > permissionsToRequest = new HashSet<>();

        for( String permission : PERMISSIONS_NEEDED ) {
            Log.i( TAG, "Permission: " + permission );
            // If the current permission hasn't been granted
            if( ContextCompat.checkSelfPermission( this, permission ) != PackageManager.PERMISSION_GRANTED ) {
                // If the user has previous denied requests to allow a permission, tell them to
                // allow said permission and exit the app.
                if( ActivityCompat.shouldShowRequestPermissionRationale( this, permission ) ) {
                    Log.i( TAG, "Permission previously denied: " + permission );
                    Toast.makeText( this, "Please allow permission " + permission, Toast.LENGTH_LONG ).show();

                    System.out.println( "exit 233" );
                    System.exit( 0 );
                    return;
                }
                // Otherwise, add the permission to the list of permissions that need to be requested
                else {
                    Log.i( TAG, "Adding permission to quest: " + permission );
                    permissionsToRequest.add( permission );
                }
            }
        }
        if( permissionsToRequest.size() > 0 ) {
            Log.i( TAG, "Permissions to request: " + permissionsToRequest.toString() );
            ActivityCompat.requestPermissions( this,
                    permissionsToRequest.toArray( new String[ permissionsToRequest.size() ] ),
                    REQUEST_CODE_PERMISSIONS_REQUEST );
        }
    }

    public void setOrientation( boolean portrait ) {
        this.orientationPortrait = portrait;
    }

    // Method to connect to the bebop drone
    @SuppressLint("NewApi")
    public void connect() {

        final MainMenuActivity MENU = this;

        // Defining an alert dialog telling the user that AnDrone is attempting to connect to the drone
        connectBuilder = new AlertDialog.Builder( this );
        connectBuilder.setMessage( "Connecting to drone..." );
        connectBuilder.setCancelable( true ); // Making it cancelable
        // Creating and assigning an OnDismissListener for the dialog to tell the user that the
        // connection was unsuccessful and possible means to fix the issue
        connectBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                // Defining an error dialog
                AlertDialog.Builder connectionError = new AlertDialog.Builder( MENU );
                connectionError.setCancelable( true ); // Making it cancelable
                connectionError.setMessage( "Error: could not find a drone on WiFi.\nPlease make sure this device is connected to the drone via WiFi and that your mobile data is turned off." );
                // Adding a button to cancel the error dialog and to call the connectFailed method
                connectionError.setNegativeButton( "Okay", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface connectionError, int i) {
                        connectionError.cancel();
                        MENU.connectFailed();
                    } } );
                // Showing the error dialog
                connectionError.create().show();
            }
        });

        // Creating and showing the alert dialog
        connectDialog = connectBuilder.create();
        connectDialog.show();

        // Setting a 15 second time to allow AnDrone to connect to the Bebop drone
        Timer timer = new Timer();
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        // If the timer runs out, dismiss the connection dialog, thus triggering the
                        // OnDismissListener on the dialog
                        connectDialog.dismiss();
                    }
                }, 15000 );

        // Creating a new BebopDiscoverer object and setting it up so it can start looking for the
        // Bebop drone
        bebopDiscoverer = new BebopDiscoverer( this, this,timer );
        bebopDiscoverer.setup();
        bebopDiscoverer.addListener( DISCOVERER_LISTENER );
        bebopDiscoverer.startDiscovering();
    }

    // Method for cleaning up the buttonListener and bebopDiscoverer objects after a failed attempt
    // to connect to the Bebop drone
    public void connectFailed() {
        // Resetting the connecting boolean flag in the button listener
        buttonListener.connecting = false;

        // Stop the disconnecting process, removing the listener and run the cleanup method in the
        // bebopDiscoverer object
        bebopDiscoverer.stopDiscovering();
        bebopDiscoverer.removeListener( DISCOVERER_LISTENER );
        bebopDiscoverer.cleanup();
        bebopDiscoverer = null;
    }

    // Method to check if AnDrone is currently attempting to connect to a Bebop drone
    public boolean isConnecting() {
        return buttonListener.connecting;
    }
}

class RadioListener implements RadioGroup.OnCheckedChangeListener {

    private static final String TAG = RadioListener.class.getSimpleName();

    private MainMenuActivity menu;
    // Integer to hold the Integer ID of the currently checked radio button in the orientation radio group
    private int isChecked;


    public RadioListener( MainMenuActivity menu ) {
        this.menu = menu;

        // Setting the portrait radio button to the currently checked radio button
        this.isChecked = R.id.radio_portrait;
    }

    @Override
    public void onCheckedChanged( RadioGroup group, int checkedId ) {

        switch (checkedId) {
            case -1:
                break;

            // If the portrait radio button is now checked
            case R.id.radio_portrait:
                // If AnDrone is currently attempting to connect to a Bebop drone
                if( menu.isConnecting() ) {
                    // Set the previously checked radio button to the the currently checked button
                    group.check( isChecked );
                } // Otherwise, set isChecked to the newly checked radio button, set the orientation
                  // to portrait and set the portrait flag in the MainMenuActivity object to true
                else {
                    isChecked = R.id.radio_landscape;
                    menu.setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT );
                    menu.setOrientation( true );
                }
                break;

            // If the landscape radio button is now checked
            case R.id.radio_landscape:
                // If AnDrone is currently attempting to connect to a Bebop drone
                if( menu.isConnecting() ) {
                    // Set the previously checked radio button to the currently checked button
                    group.check( isChecked );
                } // Otherwise, set isChecked to the newly checked radio button, set the orientation
                  // to landscape and set the portrait flag in the MainMenuActivity object to false
                else {
                    isChecked = R.id.radio_landscape;
                    menu.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    menu.setOrientation(false);
                }
                break;
        }
    }
}
