package ce301.essex.androne.menu;

import android.view.View;
import ce301.essex.androne.R;

public class MainButtonListener implements View.OnClickListener {

    private static final String TAG = MainButtonListener.class.getSimpleName();

    // Boolean flag for whether AnDrone is connecting to the Bebop drone
    public static boolean connecting = false;

    // MainMenuActivity reference
    private MainMenuActivity menu;

    public MainButtonListener( MainMenuActivity menu ) {
        this.menu = menu;
    }

    @Override
    public void onClick( View view ) {
        switch( view.getId() ) {

            // If the triggered view is the connecting button
            case R.id.connect_btn:
                // If AnDrone isn't currently connecting
                if( !connecting) {
                    // Set connecting flag to true and attempt to connect to the bebop
                    connecting = true;
                    menu.connect();
                }
                break;

            // If the triggered view is the exit button
            case R.id.exit_btn:
                // Call System.exit with code 0
                System.out.println( "exit 300" );
                System.exit( 0 );
                break;

            default:
                break;
        }
    }
}
