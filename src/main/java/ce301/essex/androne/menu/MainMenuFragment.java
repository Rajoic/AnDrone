package ce301.essex.androne.menu;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

public class MainMenuFragment extends Fragment {

    private static final String TAG = MainMenuFragment.class.getSimpleName();

    private MainMenuActivity menu = null;

    // Listeners for the main menu to be stored for state recovery
    private static MainButtonListener buttonListener;
    private static RadioListener radioListener;

    public MainMenuFragment() {}

    @Override
    public void onCreate( Bundle state) {
        super.onCreate( state );
        Log.i( TAG, "onCreate" );
        setRetainInstance( true );
    }

    // Sets an instance of the MainMenuActivity class.
    // To be called in onResume() in the MainMenuActivity class
    public void setup( @NonNull MainMenuActivity menu ) {
        Log.i( TAG, "setup" );
        this.menu = menu;
    }

    // Removed an instance of the MainMenuActivity class.
    // To be called in the onPause() in the MainMenuActivity
    public void cleanup() {
        Log.i( TAG, "cleanup" );
        this.menu = null;
    }

    // Gets an instance of the ButtonListener class held by the fragment.
    // If the instance doesn't exist yet, it is created before returning it.
    public MainButtonListener getButtonListener() {
        Log.i( TAG, "getButtonListener" );
        if( buttonListener == null & menu != null ) {
            Log.i( TAG, "getButtonListener: buttonListener && menu != null" );
            buttonListener = new MainButtonListener( this.menu );
        }
        return buttonListener;
    }

    // Gets an instance of the RadioListener class help by the fragment.
    // If the instance doesn't exist yet, it is created before returning it.
    public RadioListener getRadioListener() {
        Log.i( TAG, "getRadioListener" );
        if( radioListener == null & menu != null ) {
            Log.i( TAG, "getRadioListener: radioListenr && menu != null" );
            radioListener = new RadioListener( this.menu );
        }
        return radioListener;
    }
}
