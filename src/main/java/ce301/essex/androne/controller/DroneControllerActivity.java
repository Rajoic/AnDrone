package ce301.essex.androne.controller;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM;
import com.parrot.arsdk.arcommands.ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARCONTROLLER_DEVICE_STATE_ENUM;
import com.parrot.arsdk.arcontroller.ARControllerCodec;
import com.parrot.arsdk.arcontroller.ARFrame;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import ce301.essex.androne.drone.BebopDrone;
import ce301.essex.androne.menu.MainButtonListener;
import ce301.essex.androne.menu.MainMenuActivity;
import ce301.essex.androne.R;
import ce301.essex.androne.view.CVImageView;
import ce301.essex.androne.view.H264VideoView;

public class DroneControllerActivity extends AppCompatActivity {

    public static final String TAG = DroneControllerActivity.class.getSimpleName();

    private BebopDrone bebop; // BebopDrone object for sending commands to the drone
    private H264VideoView video; // H264VideoView object for displaying the Bebop's video stream
    private CVImageView redBallCV; // CVImageView object for finding a red ball in the Bebop's video stream
    private VelocitySeekBar seekBar;

    // Controller listener object
    private ControllerListener controllerListener;

    // AlertDialog and builder objects
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog connectingDialog;

    private ImageButton changeState; // Image button to make the drone take-off/land
    private ImageButton disconnect; // Image button to disconnect from the drone
    private ImageButton takeScreenshot; // Image button to save the current frame of the bebop's video stream to a JPG file on the android device
    private ImageButton redBall; // Image button to activate or deactivate the red ball detection function
    private ImageButton velocityShow;

    // Image buttons for left and right movement
    private ImageButton moveLeftwards;
    private ImageButton moveRightwards;

    // Image buttons for forwards and backwards movement
    private ImageButton moveForwards;
    private ImageButton moveBackwards;

    // Image buttons for altitude control
    private ImageButton increaseAltitude;
    private ImageButton decreaseAltitude;

    // Image buttons for rotation
    private ImageButton rotateClockwise;
    private ImageButton rotateAntiClockwise;

    @SuppressLint( "ClickableViewAccessibility" )
    private final BebopDrone.Listener BEBOP_LISTENER = new BebopDrone.Listener() {

        // Method to detect the changes in the state of the connection to the drone
        @Override
        public void onDroneConnectionChanged( ARCONTROLLER_DEVICE_STATE_ENUM state ) {
            Log.i( TAG, "Drone connection changed: " + state.toString() );

            switch( state ){

                // If the drone is connected
                case ARCONTROLLER_DEVICE_STATE_RUNNING:
                    // Dismiss the dialog
                    connectingDialog.dismiss();
                    break;

                // If the drone isn't running
                case ARCONTROLLER_DEVICE_STATE_STOPPED:
                    // Dismiss the dialog and close the controller activity
                    connectingDialog.dismiss();
                    finish();
                    break;
            }
        }

        // Method to get the battery level of the drone
        @Override
        public void onBatteryChargeChanged( int batteryPercentage ) {
            Log.i( TAG, "Battery charge changed: %" + batteryPercentage );
            // batteryLabel.setText( String.format( "%d&&", batteryPercentage ) );
        }

        // Method to change the state of the take-off/land button according to the state of the drone
        @Override
        public void onPilotingStateChanged( ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_ENUM state ) {
            Log.i( TAG, "Piloting state changed: " + state.toString() );

            switch( state ) {
                // If the drone is currently flying, hovering, landing or landed
                case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDING:
                case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING:
                case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_HOVERING:
                case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED:
                    // Enable the take-off/land button
                    changeState.setEnabled( true );
                    break;

                // Otherwise
                default:
                    // Disable the take-off/land button
                    changeState.setEnabled( false );
                    break;
            }
        }

        @Override
        public void onPictureTaken( ARCOMMANDS_ARDRONE3_MEDIARECORDEVENT_PICTUREEVENTCHANGED_ERROR_ENUM error ) {
            Log.i( TAG, "Picture taken" );
        }

        @Override
        public void configureDecoder( ARControllerCodec codec ) {
            Log.i( TAG, "Configuring decoder" );
            video.configureDecoder( codec );
        }

        @Override
        public void onFrameReceived( ARFrame frame ) {
            // Log.i( TAG, "Frame received" );
            video.displayFrame( frame );
        }

        @Override
        public void onMatchingMediasFound( int nbMedias ) { /* unneeded */ }

        @Override
        public void onDownloadProgressed( String mediaName, int progress ) { /* unneeded */ }

        @Override
        public void onDownloadComplete( String mediaName ) { /* unneeded */ }

    };

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setting the appropriate orientation to what was selected in the main menu
        if( MainMenuActivity.orientationPortrait ) {
            setContentView( R.layout.activity_drone_controller_portrait );
            setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT );
        }
        else {
            setContentView( R.layout.activity_drone_controller_landscape );
            setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE );
        }

        // Initialising the controller interface
        initialiseInterface();

        // Retrieving the ARDiscoveryDeviceService object corresponding to the Bebop AnDrone is
        // connected to from the intent
        Intent intent = getIntent();
        ARDiscoveryDeviceService service = intent.getParcelableExtra( MainMenuActivity.EXTRA_BEBOP_SERVICE );

        // Creating a BebopDrone object from the service object and adding a listener to it
        bebop = new BebopDrone( this, service );
        bebop.addListener( BEBOP_LISTENER );

        // Adding the bebop object to the controller listener
        controllerListener.setBebop( bebop );

        seekBar = new VelocitySeekBar( ( SeekBar ) findViewById( R.id.seekBar ), this, controllerListener );
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onWindowFocusChanged( boolean hasFocus ) {
        super.onWindowFocusChanged( hasFocus );
    }

    @Override
    protected void onResume() {
        super.onResume();
        redBallCV.resume( video );
    }

    @Override
    protected void onStart() {
        super.onStart();

        // If the bebop object isn't null and the bebop is running
        if( ( bebop != null ) && !( ARCONTROLLER_DEVICE_STATE_ENUM.ARCONTROLLER_DEVICE_STATE_RUNNING.equals( bebop.getConnectionState() ) ) ) {

            // Display a connecting dialog
            dialogBuilder = new AlertDialog.Builder( this );
            dialogBuilder.setMessage( "Connecting..." );
            dialogBuilder.setCancelable( false );
            connectingDialog = dialogBuilder.create();
            connectingDialog.show();

            // If the connection to the drone fails, finish the activity
            if( !bebop.connect() ) finish();
        }
    }

    @Override
    public void onBackPressed() {
        // If the bebop object has been initialised, disconnect from the drone
        if( bebop != null ) {
            // Create and show a dialog notifying the user that the application is disconnecting
            dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage("Disconnecting...");
            dialogBuilder.setCancelable(false);
            AlertDialog dialog = dialogBuilder.create();
            dialog.show();

            // Set the connecting flag in the main menu's button listener to false
            MainButtonListener.connecting = false;

            // Exit the controller activity if the disconnect method was successful
            if (!bebop.disconnect()) {
                dialog.dismiss();
                finish();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        redBallCV.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Dispose of the bebop object and dismiss the connecting AlertDialog
        connectingDialog.dismiss();
        bebop.dispose();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initialiseInterface() {

        // Get the video displaying view objects
        video = ( H264VideoView ) findViewById( R.id.droneCam );
        redBallCV = ( CVImageView ) findViewById( R.id.droneCamOpenCV );

        // Make the computer vision view invisible
        redBallCV.setVisibility( View.INVISIBLE );

        // Create a new controller listener
        controllerListener = new ControllerListener( this, dialogBuilder, redBallCV, video, ( SeekBar ) findViewById( R.id.seekBar ) );

        // Get the red_ball, screenshot and drone_disconnect buttons and assign the controller listener to them
        redBall = (ImageButton) findViewById( R.id.red_ball );
        takeScreenshot = ( ImageButton ) findViewById( R.id.screenshot );
        disconnect = ( ImageButton ) findViewById( R.id.drone_disconnect );
        velocityShow = ( ImageButton ) findViewById( R.id.velocity_show );

        redBall.setOnClickListener( controllerListener );
        disconnect.setOnClickListener( controllerListener );
        takeScreenshot.setOnClickListener( controllerListener );
        velocityShow.setOnClickListener( controllerListener );

        // Get the take_off_land, rotation, altitude and the move forwards, backwards, left and right buttons and assign the controller listener to them
        changeState = ( ImageButton ) findViewById( R.id.take_off_land );
        rotateAntiClockwise = ( ImageButton ) findViewById( R.id.rotate_anticlockwise );
        rotateClockwise     = ( ImageButton ) findViewById( R.id.rotate_clockwise );
        increaseAltitude = ( ImageButton ) findViewById( R.id.increase_altitude );
        decreaseAltitude = ( ImageButton ) findViewById( R.id.decrease_altitude );
        moveForwards = ( ImageButton ) findViewById( R.id.move_forwards );
        moveBackwards = ( ImageButton ) findViewById( R.id.move_backwards );
        moveLeftwards = ( ImageButton ) findViewById( R.id.move_leftwards );
        moveRightwards = ( ImageButton ) findViewById( R.id.move_rightwards );


        changeState.setOnTouchListener( controllerListener );
        rotateAntiClockwise.setOnTouchListener( controllerListener );
        rotateClockwise.setOnTouchListener( controllerListener );
        moveForwards.setOnTouchListener( controllerListener );
        moveBackwards.setOnTouchListener( controllerListener );
        moveLeftwards.setOnTouchListener( controllerListener );
        moveRightwards.setOnTouchListener( controllerListener );
        increaseAltitude.setOnTouchListener( controllerListener );
        decreaseAltitude.setOnTouchListener( controllerListener );
    }

    private void toggleCameras() {
        if( redBallCV.getVisibility() == View.INVISIBLE ) {
            Log.i( TAG, "toggleCameras: redBallCV View.INVISIBLE" );
            redBallCV.setVisibility( View.VISIBLE );
        } else {
            Log.i( TAG, "toggleCameras: redBallCV View.VISIBLE" );
            redBallCV.setVisibility( View.INVISIBLE );
        }
    }
}

class VelocitySeekBar implements SeekBar.OnSeekBarChangeListener {

    private static final String TAG = VelocitySeekBar.class.getSimpleName();

    public static final int VELOCITY_0   =   0;
    public static final int VELOCITY_10  =  10;
    public static final int VELOCITY_20  =  20;
    public static final int VELOCITY_30  =  30;
    public static final int VELOCITY_40  =  40;
    public static final int VELOCITY_50  =  50;
    public static final int VELOCITY_60  =  60;
    public static final int VELOCITY_70  =  70;
    public static final int VELOCITY_80  =  80;
    public static final int VELOCITY_90  =  90;
    public static final int VELOCITY_100 = 100;

    private SeekBar seekBar;
    private ControllerListener controllerListener;
    private final Context CONTEXT;

    public VelocitySeekBar( SeekBar seekBar, Context context, ControllerListener controllerListener ) {
        this.CONTEXT = context;
        this.seekBar = seekBar;
        this.controllerListener = controllerListener;

        seekBar.setOnSeekBarChangeListener( this );
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {}

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        Log.i( TAG, "onStopTrackingTouch: progress = " + progress );

        switch( progress ) {
            case 0:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_10" );
                controllerListener.setVelocity( VELOCITY_10 );
                break;
            case 1:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_20" );
                controllerListener.setVelocity( VELOCITY_20 );
                break;
            case 2:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_30" );
                controllerListener.setVelocity( VELOCITY_30 );
                break;
            case 3:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_40" );
                controllerListener.setVelocity( VELOCITY_40 );
                break;
            case 4:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_50" );
                controllerListener.setVelocity( VELOCITY_50 );
                break;
            case 5:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_60" );
                controllerListener.setVelocity( VELOCITY_60 );
                break;
            case 6:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_70" );
                controllerListener.setVelocity( VELOCITY_70 );
                break;
            case 7:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_80" );
                controllerListener.setVelocity( VELOCITY_80 );
                break;
            case 8:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_90" );
                controllerListener.setVelocity( VELOCITY_90 );
                break;
            case 9:
                Log.i( TAG, "onStopTrackingTouch: VELOCITY_100" );
                controllerListener.setVelocity( VELOCITY_100 );
                break;
        }
    }
}

class ControllerListener implements View.OnClickListener, View.OnTouchListener {

    private static final String TAG = ControllerListener.class.getSimpleName();

    private final Context CONTEXT;

    private static final int FLAG_DISABLE = 0;
    private static final int FLAG_ENABLE = 1;


    private DroneControllerActivity controller;
    private BebopDrone bebop;
    private CVImageView openCV;
    private H264VideoView video;
    private SeekBar seekBar;
    private int velocity = 10;

    private AlertDialog.Builder dialogBuilder;

    private Bitmap screenshot;
    private int imageCount;

    public ControllerListener(DroneControllerActivity controller, AlertDialog.Builder dialogBuilder, CVImageView openCV, H264VideoView video, SeekBar seekBar ) {
        this.CONTEXT = controller;
        this.controller = controller;
        this.dialogBuilder = dialogBuilder;
        this.openCV = openCV;
        this.video = video;
        this.seekBar = seekBar;
        imageCount = 0;
    }

    public void setBebop( BebopDrone bebop ) {
        this.bebop = bebop;
    }

    public void setVelocity( int velocity ) {
        this.velocity = velocity;
    }

    @Override
    public void onClick( View view ) {
        Log.i( TAG, "onClick" );

        switch( view.getId() ) {

            case R.id.drone_disconnect:
                Log.i( TAG, "disconnect" );

                if( bebop != null ) {
                    // Create and show a dialog notifying the user that the application is disconnecting
                    dialogBuilder = new AlertDialog.Builder( CONTEXT );
                    dialogBuilder.setMessage( "Disconnecting..." );
                    dialogBuilder.setCancelable( false );
                    AlertDialog dialog = dialogBuilder.create();
                    dialog.show();

                    MainButtonListener.connecting = false;

                    if( !bebop.disconnect() ) {
                        dialog.dismiss();
                        controller.finish();
                    }
                }
                break;

            case R.id.screenshot:
                Log.i( TAG, "takeScreenshot" );

                if( bebop == null ) {
                    Log.i( TAG, "takeScreenshot: bebop == null" );
                    break;
                }
                bebop.takePicture();
                File file = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DCIM + "/AnDrone/bebop" + imageCount + ".jpg" );
                while( file.exists() ) {
                    imageCount++;
                    file = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DCIM + "/AnDrone/bebop" + imageCount + ".jpg" );
                }
                if( openCV.getVisibility() == View.VISIBLE ) {
                    Log.i( TAG, "takeScreenshot: redBallCV View.VISIBLE" );
                    screenshot = openCV.getBitmap();
                    if( screenshot == null ) screenshot = video.getBitmap();
                }
                else {
                    Log.i( TAG, "takeScreenshot: redBallCV View.VISIBLE" );
                    screenshot = video.getBitmap();
                }

                File dir = new File( file.getParent() );
                if( !dir.exists() ) dir.mkdirs();
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    screenshot.compress( Bitmap.CompressFormat.JPEG, 100, bytes );
                    FileOutputStream fos = new FileOutputStream( file );
                    fos.write( bytes.toByteArray() );
                    fos.close();
                } catch ( Exception e ) {
                    e.printStackTrace();
                }
                break;

            case R.id.red_ball:
                Log.i( TAG, "red_ball" );

                if( openCV.getVisibility() == View.VISIBLE ) {
                    openCV.pause();
                    openCV.setVisibility( View.INVISIBLE );
                } else {
                    openCV.setVisibility( View.VISIBLE );
                    openCV.resume( video );
                }
                break;

            case R.id.velocity_show:
                Log.i( TAG, "velocity_show" );
                if( seekBar.getVisibility() == View.VISIBLE ) {
                    seekBar.setVisibility( View.INVISIBLE );
                } else if( seekBar.getVisibility() == View.INVISIBLE ) {
                    seekBar.setVisibility( View.VISIBLE );
                }
            default:
                break;
        }
    }

    @Override
    public boolean onTouch( View v, MotionEvent event ) {
        Log.i( TAG, "onTouch: velocity = " + velocity );
        switch( v.getId() ) {
            // If the view is the decrease altitude button
            case R.id.decrease_altitude:
                Log.i( TAG, "decrease_altitude" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to decrease its altitude
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setGaz( ( byte ) -velocity );
                        break;

                    // If it is no longer being pressed, tell the drone to stop decreasing its altitude
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setGaz( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        break;
                }
                break;

            // If the view is the increase altitude button
            case R.id.increase_altitude:
                Log.i( TAG, "increase_altitude" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to increase its altitude
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setGaz( ( byte ) velocity );
                        break;
                    // If it is no longer being pressed, tell the drone to stop increasing its altitude
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setGaz( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        break;
                }
                break;

            // If the view is the rotate anticlockwise button
            case R.id.rotate_anticlockwise:
                Log.i( TAG, "rotate_anticlockwise" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to rotate anticlockwise
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setYaw( ( byte ) -velocity );
                        break;
                    // If it is no longer being pressed, tell the drone to stop rotating anticlockwise
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setYaw( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        break;
                }
                break;

            // If the view is the rotate clockwise button
            case R.id.rotate_clockwise:
                Log.i( TAG, "rotate_clockwise" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to rotate clockwise
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setYaw( ( byte ) velocity );
                        break;
                    // If it is no longer being pressed, tell the drone to stop rotating clockwise
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setYaw( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        break;
                }
                break;

            // If the view is the move backwards button
            case R.id.move_backwards:
                Log.i( TAG, "move_backwards" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to move backwards
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setPitch( ( byte )  -velocity );
                        bebop.setFlag( ( byte ) FLAG_ENABLE );
                        break;
                    // If it is no longer being pressed, tell the drone to stop moving backwards
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setPitch( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        bebop.setFlag( ( byte ) FLAG_DISABLE );
                        break;
                }
                break;

            // If the view is the move forwards button
            case R.id.move_forwards:
                Log.i( TAG, "moveForwards" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to move forwards
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setPitch( ( byte ) velocity );
                        bebop.setFlag( ( byte ) FLAG_ENABLE );
                        break;
                    // If it is no longer being pressed, tell the drone to stop moving forwards
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setPitch( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        bebop.setFlag( ( byte ) FLAG_DISABLE );
                        break;
                }
                break;

            // If the view is the move leftwards button
            case R.id.move_leftwards:
                Log.i( TAG, "move_leftwards" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to move leftwards
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setRoll( ( byte ) -velocity );
                        bebop.setFlag( ( byte ) FLAG_ENABLE );
                        break;
                    // If it is no longer being pressed, tell the drone to stop moving leftwards
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setRoll( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        bebop.setFlag( ( byte ) FLAG_DISABLE );
                        break;
                }
                break;

            // If the view is the move rightwards button
            case R.id.move_rightwards:
                Log.i( TAG, "move_rightwards" );

                switch( event.getAction() ) {
                    // If it was pressed, tell the drone to move rightwards
                    case MotionEvent.ACTION_DOWN:
                        v.setPressed( true );
                        bebop.setRoll( ( byte ) velocity );
                        bebop.setFlag( ( byte ) FLAG_ENABLE );
                        break;
                    // If it is no longer being pressed, tell the drone to stop moving rightwards
                    case MotionEvent.ACTION_UP:
                        v.setPressed( false );
                        bebop.setPitch( ( byte ) VelocitySeekBar.VELOCITY_0 );
                        bebop.setFlag(  ( byte ) FLAG_DISABLE );
                        break;
                }
                break;

            // If the view is the take-off/land button
            case R.id.take_off_land:
                Log.i( TAG, "take_off_land" );
                // If it was pressed, tell the drone to change flying state
                if( event.getAction() == MotionEvent.ACTION_DOWN ) {
                    switch( bebop.getFlyingState() ) {
                        // If it's landed, take-off
                        case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_LANDED:
                            bebop.takeOff();
                            break;
                        // If it's flying or hovering, tell it to land
                        case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_FLYING:
                        case ARCOMMANDS_ARDRONE3_PILOTINGSTATE_FLYINGSTATECHANGED_STATE_HOVERING:
                            bebop.land();
                            break;
                    }
                }
                break;
        }
        return true;
    }
}
