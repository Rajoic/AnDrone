package ce301.essex.androne.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.os.Handler;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;


public class CVImageView extends android.support.v7.widget.AppCompatImageView {

    private static final String TAG = CVImageView.class.getSimpleName();
    private final Context CONTEXT;

    private Handler openCVHandler = new Handler();
    private BallThread openCVThread = null;

    private H264VideoView video = null;

    public CVImageView( Context context ) {
        this( context, null );
        Log.i( TAG, "Constructor 1" );
    }

    public CVImageView( Context context, AttributeSet attrs ) {
        this( context, attrs, 0 );
        Log.i( TAG, "Constructor 2" );
    }

    public CVImageView( Context context, AttributeSet attrs, int defStyleAttr ) {
        super( context, attrs, defStyleAttr );
        this.CONTEXT = context;
        Log.i( TAG, "Constructor 3" );
    }

    // Method to start the computer vision thread. Should be called in the onResume method of the
    // controller activity and in the listener of the red ball button. When called in onResume, if
    // the thread was previously running, it should create a new cv thread and start the algorithm
    // again. Otherwise it will do nothing. In the button listener,
    // redBallCV.setVisibility( View.VISIBLE ) should be called before this method
    public void resume( H264VideoView video ) {
        Log.i( TAG, "Resume" );
        // If this view is visible
        if( getVisibility() == View.VISIBLE ) {
            Log.i( TAG, "Is View.VISIBLE" );
            this.video = video;
            // Create a new cv thread and start it
            openCVThread = new BallThread( CONTEXT );
            openCVThread.start();
        } else Log.i( TAG, "Is View.INVISIBLE" );
    }

    // Method to stop the computer vision thread if it is running. This is to be called in the
    // onPause method of the controller activity
    public void pause() {
        Log.i( TAG, "Pause" );
        // If this is visible
        if( getVisibility() == View.VISIBLE ) {
            Log.i( TAG, "Is View.VISIBLE" );
            // Stop the computer vision thread
            openCVThread.interrupt();
            try {
                openCVThread.join();
            } catch( InterruptedException e ) {
                e.printStackTrace();
                Log.e( TAG, "Pause", e );
            }
        } else Log.i( TAG, "Is View.INVISIBLE" );
    }

    // Returns the bitmap produced from the computer vision thread
    public Bitmap getBitmap() {
        Log.i( TAG, "Getting Bitmap" );
        return openCVThread.getBitmap();
    }

    private class BallThread extends Thread {

        // Tag for Logs
        private final String TAG = BallThread.class.getSimpleName();

        // HSV HIGH AND LOW THRESHOLDING VALUES
        // Low and high hue values
        private static final int LOW_HUE_A = 170;
        private static final int HIGH_HUE_A = 180;

        private static final int LOW_HUE_B = 0;
        private static final int HIGH_HUE_B = 15;

        // Low and high saturation values
        private static final int LOW_SATURATION_A = 150;
        private static final int HIGH_SATURATION_A = 255;

        private static final int LOW_SATURATION_B = 150;
        private static final int HIGH_SATURATION_B = 255;

        // Low and high value values
        private static final int LOW_VALUE_A = 120;
        private static final int HIGH_VALUE_A = 255;

        private static final int LOW_VALUE_B = 120;
        private static final int HIGH_VALUE_B = 255;

        private final Handler handler;
        private boolean interrupted = false;
        private Bitmap bmp;

        private BallThread( Context context ) {
            Log.i( TAG, "Constuctor" );
            handler = new Handler( context.getMainLooper() );
        }

        // Returning the bitmap image
        public Bitmap getBitmap() {
            Log.i( TAG, "getBitmap(): Getting Bitmap" );
            if( bmp == null ) Log.i( TAG, "getBitmap(): Bitmap is null" );
            return bmp;
        }

        // On the next loop of the CV algorithm, stop looping
        public void interrupt() {
            Log.i( TAG, "Interrupted" );
            this.interrupted = true;
        }

        @Override
        public void run() {
            // Creating new mat objects
            final Mat firstMat = new Mat();
            final Mat mat = new Mat();

            // While this thread hasn't been interrupted
            while( !interrupted ) {
                // Get the bitmap of the current frame from the H264VideoView instance
                final Bitmap source = video.getBitmap();

                if( source != null ) {
                    // Convert the bitmap to a mat
                    Utils.bitmapToMat( source, firstMat );
                    //Imgproc.resize( firstMat, firstMat, new Size( FRAME_WIDTH, FRAME_HEIGHT ) );
                    firstMat.assignTo( mat );

                    // Convert the image
                    Imgproc.cvtColor( mat, mat, Imgproc.COLOR_RGB2HSV );
                    Mat imgThresholdA = new Mat();
                    Mat imgThresholdB = new Mat();

                    // Threshold the image twice, once for each red range in the HSV model
                    Core.inRange( mat, new Scalar( LOW_HUE_A, LOW_SATURATION_A, LOW_VALUE_A ), new Scalar( HIGH_HUE_A, HIGH_SATURATION_A, HIGH_VALUE_A ), imgThresholdA );
                    Core.inRange( mat, new Scalar( LOW_HUE_B, LOW_SATURATION_B, LOW_VALUE_B ), new Scalar( HIGH_HUE_B, HIGH_SATURATION_B, HIGH_VALUE_B ), imgThresholdB );

                    // Creating new mat which contains a combination of the 2 thresholded images
                    Mat combinedRed = new Mat();
                    Core.addWeighted( imgThresholdA, 1.0, imgThresholdB, 1.0, 0.0, combinedRed );

                    // Getting contours of the ball (hopefully)
                    ArrayList< MatOfPoint > contours = new ArrayList<>();
                    Imgproc.findContours(combinedRed, contours, combinedRed, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE );

                    // Variables to hold the max contour area value and ID for said are
                    double maxVal = 0;
                    int maxValIdx = 0;

                    // If there is at least 1 contour
                    if( contours.size() > 0 ) {
                        Log.i( TAG, "Contours found" );
                        // For each contour
                        double contourArea;
                        for( int contourIdx = 0; contourIdx < contours.size(); contourIdx++ ) {
                            // If the contour area is larger than the current max contour area
                            contourArea = Imgproc.contourArea( contours.get( contourIdx) );
                            if( maxVal < contourArea ) {
                                // Update the max and the maxId variables
                                maxVal = contourArea;
                                maxValIdx = contourIdx;
                            }
                        }
                    }

                    // Draw the largest contour on the mat
                    Imgproc.drawContours( firstMat, contours, maxValIdx, new Scalar( 0, 255, 0 ), 5 );


                    // Convert the mat back to a Bitmap for displaying or saving
                    bmp = Bitmap.createBitmap( firstMat.cols(), firstMat.rows(), Bitmap.Config.ARGB_8888 );
                    Utils.matToBitmap( firstMat, bmp );

                } else Log.i( TAG, "Source Bitmap is null" );

            }
        }

        private void runOnUiThread( Runnable r ) {
            handler.post( r );
        }
    }

    @Override
    protected void onDraw( Canvas canvas ) {
        if( getVisibility() == VISIBLE ) {
            this.setImageBitmap( openCVThread.getBitmap() );
        }
        super.onDraw( canvas );
    }
}
