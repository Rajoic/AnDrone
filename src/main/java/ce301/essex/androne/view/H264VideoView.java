package ce301.essex.androne.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

import com.parrot.arsdk.arcontroller.ARCONTROLLER_STREAM_CODEC_TYPE_ENUM;
import com.parrot.arsdk.arcontroller.ARControllerCodec;
import com.parrot.arsdk.arcontroller.ARFrame;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class H264VideoView extends TextureView implements TextureView.SurfaceTextureListener {

    private static final String TAG = H264VideoView.class.getSimpleName();
    private static final String VIDEO_MIME_TYPE = "video/avc"; // video type
    private static final int VIDEO_DEQUEUE_TIMEOUT = 33000; // timeout delay

    private MediaCodec mediaCodec; // video codec
    private Lock lock; // lock for threading

    // Frame buffer data
    private ByteBuffer spsBuffer;
    private ByteBuffer ppsBuffer;
    private ByteBuffer[] buffers;

    // Variables for bebop frame resolution
    private static final int VIDEO_WIDTH = 640;
    private static final int VIDEO_HEIGHT = 368;

    private Surface surface;

    // Configuration flags
    private boolean surfaceCreated = false;
    private boolean configured = false;


    public H264VideoView( Context context ) {
        this( context, null );
    }

    public H264VideoView( Context context, AttributeSet attrs ) {
        this( context, attrs, 0 );
    }

    public H264VideoView( Context context, AttributeSet attrs, int defStyleAttr ) {
        super( context, attrs, defStyleAttr );
        customInit();
    }

    // Initialises the view and the media codec, and adds the SurfaceTextureListener
    private void customInit() {
        Log.i( TAG, "Initialising..." );
        setSurfaceTextureListener( this );
        lock = new ReentrantLock();
        lock.lock();
        initMediaCodec( VIDEO_MIME_TYPE );
        lock.unlock();
    }

    // Displays the next frame of the bebop's camera feed
    public void displayFrame( ARFrame frame ) {
        lock.lock();

        // If this class hasn't been properly initialised, return
        if( !surfaceCreated || spsBuffer == null ) return;

        // If the media codec isn't configured yet, configure it
        if( !configured ) configureMediaCodec();

        if( mediaCodec != null  ) {
            if( configured ) {
                // Here we have either a good PFrame, or an IFrame
                int index = -1;

                try {
                    if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
                        index = mediaCodec.dequeueInputBuffer( VIDEO_DEQUEUE_TIMEOUT );
                } catch( IllegalStateException e ) {
                    Log.e( TAG, "Error while dequeue input buffer" );
                }
                // If there is at least 1 ByteBuffer in the codec
                if( index >= 0 ) {
                    // Get the buffer
                    ByteBuffer currH264Frame;
                    if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP )
                        currH264Frame = mediaCodec.getInputBuffer( index );
                    else {
                        currH264Frame = buffers[ index ];
                        currH264Frame.clear();
                    }

                    // If the buffer was retrieved successfully, get the frame data from the ARFrame
                    if( currH264Frame != null )
                        currH264Frame.put( frame.getByteData(), 0, frame.getDataSize() );

                    try {
                        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN )
                            mediaCodec.queueInputBuffer( index, 0, frame.getDataSize(), 0, 0 );
                    } catch( IllegalStateException e ) {
                        Log.e( TAG, "Error while queue input buffer" );
                    }
                }
            }

            // Try and display the last frame received
            if( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN ) {
                MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
                int outIndex;
                try {
                    outIndex = mediaCodec.dequeueOutputBuffer( info, 0 );

                    while( outIndex >= 0 ) {
                        mediaCodec.releaseOutputBuffer( outIndex, true );
                        outIndex = mediaCodec.dequeueOutputBuffer( info, 0 );
                    }
                } catch( IllegalStateException e ) {
                    Log.e( TAG, "Error while dequeue input buffer (outIndex)" );
            }}
        }
        lock.unlock();
    }

    // Configures decoder in order to display frames from the bebop drone
    public void configureDecoder( ARControllerCodec codec ) {
        lock.lock();

        // If the codec is set to encode frames in an H264 frame buffer
        if( codec.getType() == ARCONTROLLER_STREAM_CODEC_TYPE_ENUM.ARCONTROLLER_STREAM_CODEC_TYPE_H264 ) {
            // Getting the H264 codec in order to display the bebop's frame data
            ARControllerCodec.H264 codecH264 = codec.getAsH264();

            // Getting buffer data
            spsBuffer = ByteBuffer.wrap(codecH264.getSps().getByteData());
            ppsBuffer = ByteBuffer.wrap(codecH264.getPps().getByteData());
        }

        if( ( mediaCodec != null ) && ( spsBuffer != null ) ) {
            configureMediaCodec();
        }

        lock.unlock();
    }

    // Configures media codec
    @SuppressLint("NewApi")
    private void configureMediaCodec() {
        try {
            mediaCodec.stop();
            // Configuring codec with frame resolutions and
            MediaFormat format = MediaFormat.createVideoFormat( VIDEO_MIME_TYPE, VIDEO_WIDTH, VIDEO_HEIGHT );
            format.setByteBuffer( "csd-0", spsBuffer );
            format.setByteBuffer( "csd-1", ppsBuffer );

            // Configuring with video type and surface
            mediaCodec = MediaCodec.createDecoderByType( VIDEO_MIME_TYPE );
            mediaCodec.configure( format, surface, null, 0 );
            mediaCodec.start();

            // Getting the frame input buffers from the codec
            if( android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP )
                buffers = mediaCodec.getInputBuffers();

            // Codec is now configured so setting the flag
            configured = true;
        } catch( Exception e ) {
            Log.e( TAG, "configureMediaCodec", e );
        }
    }

    // Initialising the codec
    @SuppressLint( "NewApi" )
    private void initMediaCodec( String type ) {
        try {
            mediaCodec = MediaCodec.createDecoderByType( type );
        } catch ( IOException e ) {
            Log.e( TAG, "IOException", e );
        }

        if( ( mediaCodec != null ) && ( spsBuffer != null ) ) configureMediaCodec();
    }

    // Getting rid of the codec for when it isn't needed anymore
    @SuppressLint("NewApi")
    private void releaseMediaCodec() {
        if( mediaCodec != null ) {
            if( configured ) {
                mediaCodec.stop();
                mediaCodec.release();
            }
            configured = false;
            mediaCodec = null;
        }
    }

    // Recreating the surface when a new texture is available
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        this.surface = new Surface( surfaceTexture );
        surfaceCreated = true;
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) { /* unneeded */ }

    // When the texture is destroyed
    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        lock.lock();
        // Get rid of the codec
        releaseMediaCodec();
        // Get rid of the surface texture and the surface itself
        if( surfaceTexture != null ) surfaceTexture.release();
        if( this.surface != null ) this.surface.release();
        lock.unlock();

        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) { /* unneeded */ }
}
